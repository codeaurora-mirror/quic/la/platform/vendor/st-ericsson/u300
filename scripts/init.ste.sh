#!/system/bin/sh

insmod /data/caif/caif.ko

insmod /data/caif/phyif_omap2_spi.ko
insmod /data/caif/phyif_ser.ko

# Be careful to start ldiscd after inserting phyif_ser.ko!
/system/bin/ldiscd

insmod /data/caif/chnl_chr.ko
insmod /data/caif/chnl_net.ko

/data/caif/chardevconfig /dev/caifconfig /data/caif/create_devices.config

start ril-daemon
